import java.util.Scanner;
public class Count {
	private static Scanner in;
	public static int countWords(String str)
	{
		String[] words = str.split(" ");
		int count = words.length;
		return count;
	}
	public static void main(String[] args)
	{
	    in = new Scanner(System.in);
	    System.out.print("Enter a sentence: ");
	    String sentence =in.nextLine();
	    System.out.println ("Your sentence has " + countWords(sentence)+" words");
	    
	} 
	}
