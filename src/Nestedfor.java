public class Nestedfor{
	public static void main(String args[]){
		//declaring and initializing 2d array
		int arr[][]={{56,98,99},{65,76,100}};
		//printing 2d array
		for(int i=0; i<2; i++){
			for(int j=0; j<2; j++){
				System.out.print(arr[i][j]+" ");
				}
			System.out.println();
		}
	}
}